﻿/**
 * Created by Mateusz Urbanek and Katarzyna Chowanska 
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace WXMLab2
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void OnButtonClicked(object sender, EventArgs args)
        {
            var numOne = rows.Text;
            var numTwo = cols.Text;
            int iNumTwo = 0;
            int iNumOne;
            bool rc = int.TryParse(numOne, out iNumOne);
            if (rc) rc = int.TryParse(numTwo, out iNumTwo);

            if (iNumOne < 0 || iNumTwo < 0) rc = false;

            if (iNumOne * iNumTwo > 100) rc = false;

            if (rc) GenerateLayout(iNumOne, iNumTwo);

            var opt1 = String.Format("Generated layout with {0} rows and {1} cols.", iNumOne, iNumTwo);
            var opt2 = "Wrong input!";

            await DisplayAlert("Result", rc ? opt1 : opt2, "OK");
        }

        private void GenerateLayout(int rows, int cols)
        {
            Random r = new Random();
            gridLayout.Children.Clear();
            gridLayout.RowDefinitions.Clear();
            gridLayout.ColumnDefinitions.Clear();

            for (int i = 0; i < rows; i++)
            {
                gridLayout.RowDefinitions.Add(new RowDefinition());
            }

            for (int i = 0; i < cols; i++)
            {
                gridLayout.ColumnDefinitions.Add(new ColumnDefinition());
            }

            for (int i = 0; i < rows; i++)
            { 
                for (int j = 0; j < cols; j++)
                {
                    gridLayout.Children.Add(new BoxView
                    {
                        Color = Color.FromRgb(r.Next(0, 255), r.Next(0, 255), r.Next(0, 255)),
                        HorizontalOptions = LayoutOptions.Fill,
                        VerticalOptions = LayoutOptions.Fill
                    }, j, i);
                    gridLayout.Children.Add(new Label
                    {
                        Text = String.Format("Row: {0}, Col: {1}", i, j),
                        HorizontalOptions = LayoutOptions.Center,
                        VerticalOptions = LayoutOptions.Center
                    }, j, i);
                }
            }
        }
    }
}
