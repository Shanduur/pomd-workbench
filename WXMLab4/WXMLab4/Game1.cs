﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace WXMLab4
{
    public class Game1 : Game
    {
        Texture2D bar;
        Texture2D ball;
        SpriteFont font1;
        SpriteFont font2;

        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        Vector2 ballMovement;
        Vector2 ballPosition;
        Vector2 barPosition;

        bool fail = false;

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            base.Initialize();
            barPosition = new Vector2(GraphicsDevice.Viewport.Width/2 - 50, GraphicsDevice.Viewport.Height - 100);
            ballPosition = new Vector2(GraphicsDevice.Viewport.Width/2 - 20, 150);
            ballMovement = new Vector2(5, 5);
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            bar = Content.Load<Texture2D>("bar");
            ball = Content.Load<Texture2D>("ball");

            font1 = Content.Load<SpriteFont>("Font_1");
            font2 = Content.Load<SpriteFont>("Font_2");
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (Keyboard.GetState().IsKeyDown(Keys.F) && barPosition.X > 0)
                barPosition = barPosition + new Vector2(-10, 0);

            if (Keyboard.GetState().IsKeyDown(Keys.J) && barPosition.X + 100 < GraphicsDevice.Viewport.Width)
                barPosition = barPosition + new Vector2(10, 0);

            if (ballPosition.Y + 40 == barPosition.Y)
                if (ballPosition.X <= barPosition.X + 100 && ballPosition.X + 40 >= barPosition.X)
                    ballMovement = new Vector2(ballMovement.X, -ballMovement.Y);

            if (ballPosition.Y <= 0)
                ballMovement = new Vector2(ballMovement.X, -ballMovement.Y);

            if (ballPosition.X <= 0 || ballPosition.X + 40 >= GraphicsDevice.Viewport.Width)
                ballMovement = new Vector2(-ballMovement.X, ballMovement.Y);

            if (ballPosition.Y + 40 >= GraphicsDevice.Viewport.Height)
            {
                ballMovement = new Vector2(0, 0);
                fail = true;
            }

            ballPosition += ballMovement;

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            _spriteBatch.Begin();
            _spriteBatch.Draw(
                bar,
                barPosition,
                Color.White);
            _spriteBatch.Draw(
                ball, 
                ballPosition,
                Color.White);
            _spriteBatch.DrawString(
                font1, 
                string.Format("Time: {0}", gameTime.TotalGameTime),
                new Vector2(50, 50),
                Color.Green);

            if (fail)
                _spriteBatch.DrawString(
                    font2,
                    "YOU LOST",
                    new Vector2(GraphicsDevice.Viewport.Width/2 - 4*56, GraphicsDevice.Viewport.Height / 2 - 56/2),
                    Color.Red);

            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
