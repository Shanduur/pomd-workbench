﻿
namespace WXMLab4
{
    abstract class Entity
    {
        private int width;
        private int height;

        public int getWidth()
        {
            return this.width;
        }

        public int getHeight()
        {
            return this.height;
        }

        public abstract void collide(Entity e);
    }
}
