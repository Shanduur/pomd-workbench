﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace WXMLab1
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void OnButtonClicked(object sender, EventArgs args)
        {
            Button button = (Button)sender;

            bool rc = false;

            var numOne = numeroUno.Text;
            var numTwo = numeroDuo.Text;

            double dNumOne = 0, dNumTwo = 0;

            rc = double.TryParse(numOne, out dNumOne);
            if (rc) rc = double.TryParse(numTwo, out dNumTwo);

            var result = 0.0d;
            if (rc) result = dNumOne * dNumTwo;

            var opt1 = "The result of multiplication: " + result;
            var opt2 = "Wrong input!";

            await DisplayAlert("Result", rc ? opt1 : opt2,
                "OK");
        }
    }
}
