﻿using System;
using Xamarin.Forms;

namespace WXMLab3
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private async void GoToPage1(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page1());
        }

        private async void GoToPage3(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new F_Eggpalant());
        }

        private async void GoToPage4(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new F_Banana());
        }

        private async void GoToPage5(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new F_Tomato());
        }
    }
}
