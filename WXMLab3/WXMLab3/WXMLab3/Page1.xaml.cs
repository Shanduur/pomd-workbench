﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WXMLab3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public Page1()
        {
            InitializeComponent();
            Entry1.Text = ((App)(App.Current)).mytext;
        }

        private void Entry1_TextChanged(object sender, TextChangedEventArgs e)
        {
            ((App)(App.Current)).mytext = Entry1.Text;

            isl.Children.Clear();
            isl.Children.Add(
                new Label
                {
                    Text = "Image from URI"
                }
            );
            try
            {
                isl.Children.Add(new Image { Source = ImageSource.FromUri(new Uri(((App)(App.Current)).mytext)) });
            }
            catch (Exception exc)
            {
                isl.Children.Add(new Label { Text = string.Format("{0}", exc) });
            }
            isl.Children.Add(new Label { Text = string.Format("This image is downloaded from {0}", ((App)(App.Current)).mytext) });
        }
    }
}